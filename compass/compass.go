package compass

type Direction uint8

const (
	Up    Direction = 0
	Right Direction = 1
	Down  Direction = 2
	Left  Direction = 3
	None  Direction = 4
)

var Directions = []Direction{Up, Right, Down, Left}

func (d Direction) Rev() Direction {
	switch d {
	case Up:    return Down
	case Right: return Left
	case Down:  return Up
	case Left:  return Right
	default:    return None
	}
}

func (d Direction) IsRev(other Direction) bool {
	return d.Rev() == other
}
