package entity

import "github.com/nsf/termbox-go"

type Food struct {
	Coord
}

func (f *Food) Part(c Coord) rune {
	if f.Coord.IsEqual(c) {
		return FoodHeart
	}
	return Empty
}

func (f *Food) PartFg(c Coord) termbox.Attribute {
	if f.Coord.IsEqual(c) {
		return f.Fg
	}
	return termbox.ColorDefault
}
