package entity

import (
	"math/rand"
	"time"

	"bitbucket.org/azsenpai/snakegame/compass"
	"github.com/nsf/termbox-go"
)

const (
	FrameTopLeft     rune = 0x250C // "┌"
	FrameTopRight    rune = 0x2510 // "┐"
	FrameBottomLeft  rune = 0x2514 // "└"
	FrameBottomRight rune = 0x2518 // "┘"
	FrameVertical    rune = 0x2502 // "│"
	FrameHorizontal  rune = 0x2500 // "─"
	Empty            rune = 0x20   // " "
	SnakeHeadUp      rune = 0x25B2 // "▲"
	SnakeHeadRight   rune = 0x25BA // "►"
	SnakeHeadDown    rune = 0x25BC // "▼"
	SnakeHeadLeft    rune = 0x25C4 // "◄"
	SnakeBody        rune = 0x25A0 // "■"
	FoodHeart        rune = 0x2665 // "♥"
)

var fgColors = []termbox.Attribute{
	termbox.ColorRed,
	termbox.ColorGreen,
	termbox.ColorYellow,
	termbox.ColorBlue,
	termbox.ColorMagenta,
	termbox.ColorCyan,
}

type Board struct {
	Width, Height int
}

func NewBoard(w, h int) *Board {
	tw, th := termbox.Size()

	if w > tw {
		w = tw
	}

	if h > th && th-4 > 0 {
		h = th - 4
	}

	return &Board{w, h}
}

func (b *Board) NewSnake(l int) *Snake {
	snake := &Snake{
		d:      compass.Right,
		coords: make([]Coord, 0, (b.Width-2)*(b.Height-2)),
	}

	var c Coord
	rand.Seed(time.Now().Unix())

	for {
		c = b.Rand()

		if c.X-l > 0 {
			break
		}
	}

	for i := 0; i < l; i++ {
		snake.coords = append(snake.coords, Coord{
			X:  c.X - i,
			Y:  c.Y,
			Fg: termbox.ColorDefault,
		})
	}

	return snake
}

func (b *Board) NewFood(snake *Snake) *Food {
	var c Coord

	for {
		c = b.Rand()
		if snake.Part(c) == Empty {
			break
		}
	}

	if snake.Len()%4 == 0 {
		i := rand.Intn(len(fgColors))
		c.Fg = fgColors[i]
	}

	return &Food{c}
}

func (b *Board) Rand() Coord {
	x := rand.Intn(int(b.Width)-2) + 1
	y := rand.Intn(int(b.Height)-2) + 1

	return Coord{
		X: x, Y: y,
		Fg: termbox.ColorDefault,
	}
}

func (b *Board) MoveSnake(snake *Snake) {
	if len(snake.coords) == 0 {
		return
	}
	head := snake.coords[0]
	b.MoveSnakeHead(&head, snake.d)

	for i := len(snake.coords) - 1; i > 0; i-- {
		fg := snake.coords[i].Fg
		snake.coords[i] = snake.coords[i-1]
		snake.coords[i].Fg = fg
	}

	snake.coords[0] = head
}

func (b *Board) MoveSnakeHead(c *Coord, d compass.Direction) {
	c.X += delta[d].X
	c.Y += delta[d].Y

	switch {
	case c.X <= 0:
		c.X = b.Width - 2
	case c.X >= b.Width-1:
		c.X = 1
	}

	switch {
	case c.Y <= 0:
		c.Y = b.Height - 2
	case c.Y >= b.Height-1:
		c.Y = 1
	}
}

func (b *Board) Draw(snake *Snake, food *Food) {
	for y := 0; y < b.Height; y++ {
		for x := 0; x < b.Width; x++ {
			r := Empty
			fg := termbox.ColorDefault

			switch {
			case x == 0 && y == 0:
				r = FrameTopLeft
			case x == b.Width-1 && y == 0:
				r = FrameTopRight
			case x == 0 && y == b.Height-1:
				r = FrameBottomLeft
			case x == b.Width-1 && y == b.Height-1:
				r = FrameBottomRight
			case x == 0 || x == b.Width-1:
				r = FrameVertical
			case y == 0 || y == b.Height-1:
				r = FrameHorizontal
			default:
				c := Coord{X: x, Y: y}
				p := snake.Part(c)

				if p != Empty {
					r = p
					fg = snake.PartFg(c)
				} else {
					r = food.Part(c)
					fg = food.PartFg(c)
				}
			}

			termbox.SetCell(x, y, r, fg, termbox.ColorDefault)
		}
	}

	termbox.Flush()
}

func WriteText(x, y int, s string) {
	for _, v := range s {
		termbox.SetCell(x, y, v, termbox.ColorDefault, termbox.ColorDefault)
		x++
	}
}
