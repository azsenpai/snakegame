package entity

import (
	"bitbucket.org/azsenpai/snakegame/compass"
	"github.com/nsf/termbox-go"
)

var snakeHead = map[compass.Direction]rune{
	compass.Up:    SnakeHeadUp,
	compass.Right: SnakeHeadRight,
	compass.Down:  SnakeHeadDown,
	compass.Left:  SnakeHeadLeft,
	compass.None:  Empty,
}

type Snake struct {
	d      compass.Direction
	coords []Coord
}

func (s *Snake) SetDirection(d compass.Direction) {
	if d.IsRev(s.d) {
		return
	}
	s.d = d
}

func (s *Snake) Part(c Coord) rune {
	for i, v := range s.coords {
		if !c.IsEqual(v) {
			continue
		}
		switch {
		case i == 0:
			return snakeHead[s.d]
		default:
			return SnakeBody
		}
	}
	return Empty
}

func (s *Snake) PartFg(c Coord) termbox.Attribute {
	for _, v := range s.coords {
		if !c.IsEqual(v) {
			continue
		}
		return v.Fg
	}
	return termbox.ColorDefault
}

func (s *Snake) Len() int {
	return len(s.coords)
}

func (s *Snake) IsEat(f *Food) bool {
	if len(s.coords) == 0 {
		return false
	}
	return s.coords[0].IsEqual(f.Coord)
}

func (s *Snake) Inc(f *Food) {
	if len(s.coords) < 2 {
		return
	}
	t1 := s.coords[len(s.coords)-1]
	t2 := s.coords[len(s.coords)-2]

	d := t1.Direction(t2)

	if d == compass.None {
		return
	}

	t := t1.Move(d.Rev())
	t.Fg = f.Fg

	s.coords = append(s.coords, t)
}

func (s *Snake) IsDie() bool {
	if len(s.coords) == 0 {
		return true
	}
	head := s.coords[0]
	for _, c := range s.coords[1:] {
		if head.IsEqual(c) {
			return true
		}
	}
	return false
}
