package entity

import (
	"bitbucket.org/azsenpai/snakegame/compass"
	"github.com/nsf/termbox-go"
)

type Coord struct {
	X, Y int
	Fg   termbox.Attribute
}

func (c Coord) Direction(t Coord) compass.Direction {
	for _, d := range compass.Directions {
		if c.Move(d).IsEqual(t) {
			return d
		}
	}
	return compass.None
}

func (c Coord) Move(d compass.Direction) Coord {
	return Coord{
		X:  c.X + delta[d].X,
		Y:  c.Y + delta[d].Y,
		Fg: c.Fg,
	}
}

func (c Coord) IsEqual(o Coord) bool {
	return c.X == o.X && c.Y == o.Y
}

var delta = map[compass.Direction]Coord{
	compass.Up:    {X: 0, Y: -1},
	compass.Right: {X: 1, Y: 0},
	compass.Down:  {X: 0, Y: 1},
	compass.Left:  {X: -1, Y: 0},
	compass.None:  {X: 0, Y: 0},
}
