package main

import (
	"fmt"
	"time"

	"bitbucket.org/azsenpai/snakegame/compass"
	"bitbucket.org/azsenpai/snakegame/entity"
	"github.com/nsf/termbox-go"
)

func main() {
	err := termbox.Init()

	if err != nil {
		panic(err)
	}

	defer termbox.Close()

	dirs := make(chan compass.Direction)
	go startGame(dirs)

	for stop := false; !stop; {
		event := termbox.PollEvent()

		if event.Type == termbox.EventKey {
			switch event.Key {
			case termbox.KeyArrowUp:
				dirs <- compass.Up
			case termbox.KeyArrowRight:
				dirs <- compass.Right
			case termbox.KeyArrowDown:
				dirs <- compass.Down
			case termbox.KeyArrowLeft:
				dirs <- compass.Left
			case termbox.KeyEsc:
				stop = true
			}
		} else if event.Type == termbox.EventInterrupt {
			stop = true
		}
	}

	close(dirs)

	time.Sleep(time.Second * 3)
}

func startGame(dirs chan compass.Direction) {
	board := entity.NewBoard(32, 16)
	snake := board.NewSnake(3)
	food := board.NewFood(snake)

	minSpeed := time.Millisecond * 32
	speed := time.Millisecond * 64

	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	termbox.Flush()

	for {
		select {
		case d, ok := <-dirs:
			if !ok {
				return
			}
			snake.SetDirection(d)
		default:
		}

		board.MoveSnake(snake)

		if snake.IsEat(food) {
			snake.Inc(food)
			food = board.NewFood(snake)

			if snake.Len()%4 == 0 && speed-time.Millisecond >= minSpeed {
				speed -= time.Millisecond
			}
		}

		board.Draw(snake, food)

		entity.WriteText(0, board.Height+2, fmt.Sprintf("Snake Lenght: %d", snake.Len()))
		entity.WriteText(0, board.Height+3, "Please press ESC to exit")

		if snake.IsDie() {
			termbox.Interrupt()
			return
		}

		time.Sleep(speed)
	}
}
